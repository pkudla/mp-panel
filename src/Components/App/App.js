import React, { Component } from 'react';
import Header from "../Header/header";
import MainContent from "../MainContent/mainContent";

class App extends Component {
  render() {
    return (
      <>
        <Header />
        <MainContent/>
      </>
    );
  }
}

export default App;
