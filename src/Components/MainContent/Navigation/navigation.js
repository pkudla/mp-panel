import React from 'react';
import { Link } from 'react-router-dom';
import './navigation.scss';

const Navigation = () => {
    return (
        <nav className="navBar">
            <ul>
                <li><Link to="/">Dashboard</Link></li>
                <li><Link to="/customers">Klienci</Link></li>
                <li><Link to="/workers">Pracownicy</Link></li>
                <li><Link to="/orders">Zlecenia</Link></li>
                <li><Link to="/history">Historia zleceń</Link></li>
                <li><Link to="/contact">Kontakt</Link></li>
                <li><Link to="/aboutus">O nas</Link></li>
            </ul>
        </nav>
    )
};

export default Navigation;