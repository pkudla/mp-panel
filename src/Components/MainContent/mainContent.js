import React, { Component } from 'react';
import './mainContent.scss'
import Dashboard from "./Pages/Dashboard/dashboard";
import Contact from "./Pages/Contact/contact";
import {Route, BrowserRouter as Router} from 'react-router-dom';
import Navigation from "./Navigation/navigation";
import Customers from "./Pages/Customers/customers";
import Workers from "./Pages/Workers/workers";
import Orders from "./Pages/Orders/orders";
import OrdersHistory from "./Pages/OrdersHistory/ordersHistory";
import AboutUs from "./Pages/AboutUs/aboutUs";

class MainContent extends Component {
    render() {
        return (
           <main>
               <Router>
                   <>
                       <Navigation/>
                       <section>
                           <Route path="/" exact component={Dashboard}/>
                           <Route path="/customers" exact component={Customers}/>
                           <Route path="/workers" exact component={Workers}/>
                           <Route path="/orders" exact component={Orders}/>
                           <Route path="/history" exact component={OrdersHistory}/>
                           <Route path="/contact" component={Contact} />
                           <Route path="/aboutus" component={AboutUs} />
                       </section>
                   </>
               </Router>
           </main>
        )
    }
}

export default MainContent;