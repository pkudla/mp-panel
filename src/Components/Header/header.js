import React from 'react';
import logo from '../../Images/SVG/mparticle.svg';
import './header.scss';

const Header = () => {
    return (
        <>
            <header className="topBar">
                <picture>
                    <img src={logo} alt='MP-System' />
                </picture>
                <a href="foo">Wyloguj</a>
            </header>
        </>
    )
};

export default Header;